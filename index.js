// @ts-check
'use strict';

process.chdir(__dirname);
require('ts-node').register({ transpileOnly: true });

// let webhookEndpoint = process.env.TELEGRAM_WEBHOOK_LOCAL;

// Utils
const { logError } = require('./utils/log');
const { config } = require('./utils/config');
require('./server')
const bot = require('./bot');

bot.use(
	require('./handlers/middlewares'),
	require('./plugins'),
	require('./handlers/commands'),
	require('./handlers/regex'),
	require('./handlers/unmatched'),
);

bot.catch(logError);

// No more bot.launch(), because we're using webhooks. Yay!
if (process.env.NODE_ENV === 'development') {
	bot.polling.offset = -1;
} else {
	// do nothing
}

console.log('[INFO] Typescript to Javascipt compling completed in ' + process.uptime());

// eslint-disable-next-line @typescript-eslint/no-floating-promises
bot.launch();
