# Watchdog Chat Guard

**NOTE: The Watchdog Chat Guard is in beta phase;**
**it has known issues, but it's successfully being used in production**

If you need help with using the Bot or setting it up, join our [developers' lounge](https://t.me/thepinsteam_devopschat) or [the official WCG Support Chat](https://t.me/WatchdogChatGuardSupport).

If you prefer to use Watchdog Chat Guard without any changes made by the Pins team,, we recommend use [the original source code](https://github.com/thedevs-network/theguard-bot) instead, maintained by the Devs Network.

## Setup
You need [Node.js](https://nodejs.org/) (>= 12) and/or an public-reachable domain for webhooks/API endpoints to run this bot.

1. Create a bot via [@BotFather](https://t.me/BotFather) and grab a **token**.
2. Clone this repository with `git clone https://gitlab.com/MadeByThePinsTeam-DevLabs/watchdog-chat-guard` or
[download latest zip from Glitch](https://api.glitch.com/project/download/?authorization=3f9c54e6-8c9d-46ff-bd96-5a264751a49e&projectId=89ee45dd-9f62-43b6-90c3-dd04c204b4cd). Install the dependencies with `npm i`.
3. Edit the `config.js` with your values. Please READ the comments carefully as you edit.
4. Next, copy `.env.example` to `.env` and edit. If you don't have `dotenv` installed, you need to expose them into console manually.
5. Optionally install Node Version Manager and run `nvm install 12.14.1 --reinstall-packages-from=system --latest-npm`.
6. Start the bot via `npm start` and start setting up.

### With Glitch
1. [Remix the code here.](https://glitch.com/edit/#!/remix/watchdog-chat-guard)
2. Grab your **Bot API Token** from [@BotFather](https://t.me/BotFather) and paste in `.env` as `TELEGRAM_API_TOKEN`.
4. For the SpamWatch integration, you must have a API key for `https://api.spamwat.ch` or your SpamWatch API server instance. If you have these for `api.spamwat.ch`,
5. Volia! Now, set your bot up in Telegram.

### Dockerizing?
You need to have [Docker Engine (Community Edition)](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#install-from-a-package) installed on your machine in order to use this code with Docker containers.

#### Build It Yourself
Because our code is open-source and everyone with Docker CE can reproduce our Docker images, you need to do the following.

1. Create a bot via [@BotFather](https://t.me/BotFather) and grab a **token**.
2. Clone this repository with `git clone https://gitlab.com/MadeByThePinsTeam-DevLabs/watchdog-chat-guard` or
[download latest zip from Glitch](https://api.glitch.com/project/download/?authorization=3f9c54e6-8c9d-46ff-bd96-5a264751a49e&projectId=89ee45dd-9f62-43b6-90c3-dd04c204b4cd).
3. Edit our `config.js` to your values. Don't forget the two required variables to use me!
4. Run `docker build -t watchdog-chat-guard .` to build the image.
5. Run the following script to start the bot.

```bash
docker run -v $(pwd)/data:/app/data --rm -itd 3000:3000 the_guard_bot \
  \ -e SPAMWATCH_API_HOST=https://api.spamwat.ch
  \ -e SPAMWATCH_API_TOKEN=ReplaceWithYourAPITokenHere
  \ -e TELEGRAM_API_TOKEN=ReplaceWithYourBotTokenHere
```

## Features
* Synchronized database across multiple groups.
* Adding admins to the bot.
* Auto-remove and warn channels and groups ads.
* Kick bots added by users.
* Warn and ban users to control the group.
* Commands work with replying, mentioning and ID.
* Removes commands and temporary bot messages.
* Ability to create custom commands.
* Supports plugins.
* Automatically ban users when found banned in global banlists like SpamWatch. (see the docs for details!)

Overall, keeps the groups clean and healthy to use.

## Commands
Command                 | Who can use it? | Works in | Description
----------------------- | ----------      | ------------ | -----------------
`/admin`                | _Master_        | _Everywhere_ | Makes the user admin in the bot and groups.
`/unadmin`              | _Master_        | _Everywhere_ | Demotes the user from admin list.
`/leave <name\|id>`     | _Master_        | _Everywhere_ | Make the bot to leave the group cleanly.
`/hidegroup`            | _Master_        | _Groups_     | Revoke invite link and hide the group from `/groups` list.
`/showgroup`            | _Master_        | _Groups_     | Make the group accessible via `/groups` list.
`/del [reason]`         | _Admin_         | _Everywhere_ | Deletes replied-to message.
`/warn <reason>`        | _Admin_         | _Groups_     | Warns the user.
`/unwarn`               | _Admin_         | _Everywhere_ | Removes the last warn from the user.
`/nowarns`              | _Admin_         | _Everywhere_ | Clears warns for the user.
`/permit`               | _Admin_    | _Everywhere_ | Permits the user to advertise once, within 24 hours.
`/ban <reason>`         | _Admin_         | _Groups_     | Bans the user from groups.
`/unban`                | _Admin_         | _Everywhere_ | Removes the user from ban list.
`/user`                 | _Admin_         | _Everywhere_ | Shows the status of the user.
`/save <name>`          | _Admin_         | _PMs_        | Create a custom command.
`/clear <name>`         | _Admin_         | _PMs_        | Remove a custom command.
`/staff`                | _Everyone_      | _Everywhere_ | Shows a list of admins.
`/link`                 | _Everyone_      | _Everywhere_ | Shows the current group's link.
`/groups`               | _Everyone_      | _Everywhere_ | Shows a list of groups which the bot is admin in.
`/report`               | _Everyone_      | _Everywhere_ | Reports the replied-to message to admins.
`/commands`             | _Everyone_      | _PMs_        | Shows a list of available built-in commands.
`/notes`                | _Everyone_      | _PMs_        | Shows a list of available custom commands.
`/help`                 | _Everyone_      | _PMs_        | How to use the bot.
`/start`                | _Everyone_      | _PMs_        |

All commands and actions are synchronized across all of the groups managed by the the bot owner (or the bot master/s) and they work with **replying**, **mentioning** or **ID** of a user.

If used by reply, `/ban` and `/warn` would remove the replied-to message.

The bot is still in beta phase so feel free to [open issues](https://gitlab.com/MadeByThePinsTeam-DevLabs/watchdog-chat-guard/issues/new) and ask for features.

## Where are the docs?

<https://watchdog-chat-guard-docs.herokapp.com>

> **Note**: We have been moved our documentation site to Heroku due to some starting up issues on Glitch.

---

> Important Note: Because the original code is in the AGPL-3.0 license and if if you're running your own instance, you should add a link to the source [(this repository)](https://github.com/MadeByThePinsTeam-DevLabs/watchdog-chat-guard) in your bot' bio or where users can access it. If you're modifying this source and making your own bot, you should link to the source of your own version of the bot according to the AGPL-3.0 license. Check [LICENSE](LICENSE) for more info.
