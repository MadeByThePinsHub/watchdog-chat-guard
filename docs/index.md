# Yikes, we're now been moved to a better place

We move most of our documentation into a new place, now powered in Wiki.js on Glitch.

[Visit the new docs site today!](https://watchdog-chat-gurad-docs.glitch.me/)

## Why we had been moved

Making our code more cleaner than ever by seperating our docs from the source code.
