const express = require('express');
const app = express();
const { config } = require('./config');
const port = process.env.PORT || 3000

app.get('/', (req, res) => {
	res.send({ ok: true, description: 'Hello, world!', status: 200 });
});

app.get('/ping', (req, res) => {
	res.send({ ok: true, description: 'Pong!' });
});

app.use(function (req, res, next) {
  res.status(404).json({ ok: false, description: "That's didn't found in our API server."})
});

app.listen(port, () => {
	console.log('[INFO] Your app is listening on port 3000 after ' + process.uptime());
});