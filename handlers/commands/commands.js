'use strict';

const R = require('ramda');

// cfg
const { isMaster } = require('../../utils/config');

const { scheduleDeletion } = require('../../utils/tg');

const masterCommands = `\
<b>Bot master commands</b>:
<code>/admin</code> - Makes the user admin.
<code>/unadmin</code> - Demotes the user from admin list.
<code>/leave &lt;name|id&gt;</code> - Makes the bot leave the group cleanly.
<code>/hidegroup</code> - Hide the group from <code>/groups</code> list.
<code>/showgroup</code> - Show the group it in <code>/groups</code> list.\n
`;

const adminCommands = `\
<b>Network admin commands</b>:
<code>/del [reason]</code> - Deletes replied-to message.
<code>/warn &lt;reason&gt;</code> - Warns the user.
<code>/unwarn</code> - Removes the last warn from the user.
<code>/nowarns</code> - Clears warns for the user.
<code>/permit</code> - Permits the user to advertise once, within 24 hours.
<code>/ban &lt;reason&gt;</code> - Bans the user from the network.
<code>/unban</code> - Unbans the user from the network.
<code>/user</code> - Shows user's status and warns.
<code>/save &lt;name&gt;</code> - Add a new custom command/note.
<code>/clear &lt;name&gt;</code> - Nuke a custom command/note from database.\n
`;
const userCommands = `\
<b>Commands for everyone</b>:
<code>/staff</code> - Pulls the list of network admins from database.
<code>/link</code> - Get the current chat's invite link, only in groups.
<code>/groups</code> - Show the up-to-date community network list.
<code>/notes</code> - Show the network's customized notes. <b>(work in progress)</b>
<code>/report</code> - Reports the replied-to message to admins.
<code>/source</code> - Get the link to this source code of thisWatchdog Chat Guard instance.\n
`;
const role = R.prop('role');
const name = R.prop('name');

/** @param { import('../../typings/context').ExtendedContext } ctx */
const commandReferenceHandler = async (ctx) => {
	const customCommandsText = masterCommands.repeat(isMaster(ctx.from)) +
		adminCommands.repeat(ctx.from && ctx.from.status === 'admin') +
		userCommands;

	return ctx.replyWithHTML(customCommandsText)
		.then(scheduleDeletion());
};

module.exports = commandReferenceHandler;
