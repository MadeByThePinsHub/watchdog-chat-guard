'use strict';
const { Markup } = require('telegraf');
const { homepage } = require('../../package.json');
const remixOnGlitch = 'https://glitch.com/edit/#!/remix/watchdog-chat-guard';

const message = `\
Hey there!

I'm an <b>administration</b> bot that helps you to keep \
your <b>groups</b> safe from <b>spammers.</b>

Send /commands to get the list of available commands.

If you want to use me for your groups, \
note that I'm more useful on a network of groups and \
you also need to <b>setup a new bot.</b>

If you hate self-hosting, we recommend to use \
@MissRose_bot instead.
`;

/** @param { import('../../typings/context').ExtendedContext } ctx */
const helpHandler = ({ chat, replyWithHTML }) => {
	if (chat.type !== 'private') return null;

	return replyWithHTML(
		message,
		Markup.inlineKeyboard([
			Markup.urlButton('Use @thedevs\' code', homepage),
			Markup.urlButton('Remix me on Glitch', remixOnGlitch),
		]).extra(),
	);
};

module.exports = helpHandler;
