// @ts-check
'use strict';

// Utils
const { displayUser, scheduleDeletion } = require('../../utils/tg');
const { html } = require('../../utils/html');
const { parse, strip } = require('../../utils/cmd');
const { pMap } = require('../../utils/promise');

// DB
const { listGroups } = require('../../stores/group');
const { getUser, unmute } = require('../../stores/user');

/** @param { import('../../typings/context').ExtendedContext } ctx */
const unmuteHandler = async (ctx) => {
	if (ctx.from?.status !== 'admin') return null;

	const { targets } = parse(ctx.message);

	if (targets.length !== 1) {

		return ctx.replyWithHTML(
			'ℹ️ <b>I need to know who to unmute.</b>',
		).then(scheduleDeletion());
	}

	const userToUnmute = await getUser(strip(targets[0]));

	if (!userToUnmute) {
		return ctx.replyWithHTML(
			'❓ <b>I don\'t know about that user.</b>',
		).then(scheduleDeletion());
	}


	if (userToUnmute.status !== 'banned') {
		return ctx.replyWithHTML('ℹ️ <b>That user was unmuted.</b>');
	}

	await pMap(await listGroups({ type: 'supergroup' }), (group) =>
		ctx.tg.unbanChatMember(group.id, userToUnban.id));

	await unbmute(userToUnmute);

	ctx.tg.sendMessage(
		userToUnban.id,
		'♻️ You were unmuted from the Community Network. You can speak in the network.',
	).catch(() => null);
	// it's likely that the banned person haven't PMed the bot,
	// which will cause the sendMessage to fail,
	// hance .catch(noop)
	// (it's an expected, non-critical failure)


	return ctx.loggedReply(html`
		♻️ ${ctx.from.first_name} <b>unmuted</b> ${displayUser(userToUnban)}.
	`);
};

module.exports = unmuteHandler;
