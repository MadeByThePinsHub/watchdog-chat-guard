'use strict';

// Utils
const { displayUser, scheduleDeletion } = require('../../utils/tg');
const { html } = require('../../utils/html');
const { parse, strip, substom } = require('../../utils/cmd');

// Bot

// DB
const { getUser } = require('../../stores/user');

/** @param { import('../../typings/context').ExtendedContext } ctx */
const muteHandler = async (ctx) => {
	if (!ctx.message.chat.type.endsWith('group')) {
		return ctx.replyWithHTML(
			'ℹ️ <b>This command is only available in groups.</b>',
		);
	}

	if (ctx.from.status !== 'admin') return null;

	const { flags, targets, reason } = parse(ctx.message);

	if (targets.length === 0) {
		return ctx.replyWithHTML(
			'ℹ️ <b>Requires atleast one user to get muted.</b>',
		).then(scheduleDeletion());
	}

	if (reason.length === 0) {
		return ctx.replyWithHTML('ℹ️ <b>Please tell the bot why you want to mute this user.</b>')
			.then(scheduleDeletion());
	}

	if (targets.length > 1) {
		return ctx.batchMute({ admin: ctx.from, reason, targets });
	}

	const userToMute = await getUser(strip(targets[0])) || targets[0];

	if (!userToMute.id) {
		return ctx.replyWithHTML(
			'❓ <b>User unknown.</b>\n' +
			'Please forward their message, then try again.',
		).then(scheduleDeletion());
	}

	if (userToMute.id === ctx.botInfo.id) return null;

	if (userToMute.status === 'admin') {
		return ctx.replyWithHTML('ℹ️ <b>Can\'t ban other admins.</b>');
	}

	if (ctx.message.reply_to_message) {
		ctx.deleteMessage(ctx.message.reply_to_message.message_id)
			.catch(() => null);
	}

	if (!flags.has('amend') && userToMute.status === 'muted') {
		return ctx.replyWithHTML(
			html`🚫 ${displayUser(userToMute)} <b>is already muted.</b>`,
		);
	}

	return ctx.mute({
		admin: ctx.from,
		reason: await substom(reason),
		userToMute,
	});
};

module.exports = muteHandler;
