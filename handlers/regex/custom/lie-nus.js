'use strict';

// As usual, we need Telegraf
const { hears } = require('telegraf');
// Then some sort of regex handler
const XRegExp = require('xregexp');

const { replyId } = require('../../utils/tg');

// Linus lied, so uninstall YouTube LOL.
const AnswerToLiesAndLies = `\
<b>Q: Is Linus Sebastian lie?</b> \
\
A: We don't know, but many people think he lied. What the actual f***.

See also: <a href="https://kutt.it/LienusRedditThread">This Reddit thread</a> \
and from <a href="https://kutt.it/ThePinsTeamHandbook_LienusFAQs">our handbook</a>
`;

// Don't do some
const $regex = XRegExp.tag('nix')`/(Is|Did)\s+(Linus|Linus Sebastian)\s+(lie|lying)/i`;

const handler = async (ctx, next) => {
	const AskLinusQuestions = ctx.match;
	if (AskLinusQuestions === $regex) {
		ctx.replyWithHTML(AnswerToLiesAndLies);
	}

};

// Export stuff
module.exports = hears(regex, handler);
