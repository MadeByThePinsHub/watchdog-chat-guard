'use strict';

// As usual, we need Telegraf
const { hears } = require('telegraf');
// Then some sort of regex
const XRegExp = require('xregexp');

const { replyId } = require('../../utils/tg');

// Then do Linus Sebastian roasting here as handling stuff
const handler = async (ctx, next) => {

	// Add some Sawada stuff here
	const $regex = XRegExp.tag('nix')`(^|/\s?)
    ?
	($|\s?/)`;
};

// Export stuff
module.exports = hears(regex, handler);
