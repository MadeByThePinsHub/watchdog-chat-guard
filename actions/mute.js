'use strict';

const { displayUser } = require('../utils/tg');
const { html } = require('../utils/html');
const { pMap } = require('../utils/promise');
const { telegram } = require('../bot');

const { listVisibleGroups } = require('../stores/group');
const { muteUser } = require('../stores/user');

module.exports = async ({ admin, reason, userToBan }) => {
	// move some checks from handler here?

	const by_id = admin.id;
	const date = new Date();

	await muteUser(userToBan, { by_id, date, reason });

	await pMap(await listVisibleGroups(), group =>
		telegram.kickChatMember(group.id, userToBan.id));

	return html`
		🚫 ${admin.first_name} <b>muted</b> ${displayUser(userToBan)}.
		<b>Reason</b>: ${reason}
	`;
};
