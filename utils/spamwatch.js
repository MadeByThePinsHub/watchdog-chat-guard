// @ts-check
'use strict';

const { Client } = require('spamwatch');

const { config } = require('./config');

const isSpamWatchAPInetgrationEnabled = config.spamwatch;
if (config.spamwatch == false) {
	console.warn('[WARN] SpamWatch API integration disabled, spammers that banned in SpamWatch could get into your grooups!');
}

const SpamWatchAPIHost = config.spamwatch.host || 'https://api.spamwat.ch';

if (!config.spamwatch.host) {
	console.warn('[WARN] Falling back to the default SpamWatch API host...');
}

const SpamWatchAPIKey = config.spamwatch.token;
if (!config.spamwatch.token) {
	console.warn('[WARN] I cannot ban users without your API key for ' + SpamWatchAPIHost + '.');
	const SpamWatchAPIkey = undefined;
}

// eslint-disable-next-line func-names
exports.shouldKick = (function () {
	if (!config.spamwatch) {
		return () => false;
	}

	const client = new Client(SpamWatchAPIKey, SpamWatchAPIHost);
	return ({ id }) => client.getBan(id);
}());
