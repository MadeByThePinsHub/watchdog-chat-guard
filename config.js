// @ts-check
'use strict';

/**
 * @typedef { import('./typings/config').Config } Config
 * @typedef { import('./typings/config').InlineKeyboard } InlineKeyboard
 */

/*
 * If you remix me, pleae edit this file instead
 * of starting from strach.
 *
 * Config file in JSON format (`config.json`) is also supported.
 * For backwards compatibility, and because why not, it needs no extra code.
 */

/**
 * Millisecond
 * String to be parsed by https://npmjs.com/millisecond,
 * or number of milliseconds. Pass 0 to remove immediately.
 * @typedef {( number | string )} ms
 */

/**
 * @type {Config}
 */
const config = {

	/**
   * @type {!( number | string | (number|string)[] )}
   * ID (number) or username (string) of master,
   * the person who can promote and demote admins,
   * and add the bot to groups.
   *
   * Edit this one with your own values.
   * And we recommend to use IDs instead.
   */
	master: [ 705519392, 709590349 ],

	/**
   * @type {!string}
   * Telegram Bot token obtained from https://t.me/BotFather.
   *
   * You should pull your bot token from env, instead of pasting it here.
   * That's bad for your bot and your groups!
   */
	token: process.env.TELEGRAM_API_TOKEN,

	chats: {

		/**
     * @type {(number | false)}
     * Chat to send member join/leave notifications to.
     * Pass false to disable this feature.
     */
		presenceLog: -1001273802813,

		/**
     * @type {(number | false)}
     * Chat to send report notifications to.
     * Pass false to disable this feature.
     */
		report: -1001273802813,
	},

	/**
   * @type {( 'all' | 'own' | 'none' )}
   * Which messages with commands should be deleted?
   * Defaults to 'own' -- don't delete commands meant for other bots.
   */
	deleteCommands: 'own',

	deleteCustom: {
		longerThan: 450, // UTF-16 characters
		after: '3 minutes',
	},

	/**
   * @type {(ms | false)} Millisecond
   * Timeout before removing join and leave messages.
   * [Look at typedef above for details.]
   * Pass false to disable this feature.
   */
	deleteJoinsAfter: '2 minutes',

	/**
   * @type {(ms | { auto: (ms | false), manual: (ms | false) } | false)}
   * Timeout before removing auto-warn messages.
   * [Look at typedef above for details.]
   * Pass an object with { auto, manual } for more granular control
   * over which messages get deleted
   * Pass false to disable this feature.
   */
	deleteWarnsAfter: false,

	/**
   * @type {(ms | false)}
   * Timeout before removing ban messages.
   * [Look at typedef above for details.]
   * Pass false to disable this feature.
   */
	deleteBansAfter: false,

	/**
   * @type {string[]}
   * List of blacklisted domains.
   * Messages containing blacklisted domains will automatically be warned.
   * If the link is shortened, an attempt will be made to resolve it.
   * If resolved link is blacklisted, it will be warned for.
   */
	blacklistedDomains: [ 'gab.com', 'tinyurl.com' ],

	/**
   * @type {( string[] | false )}
   * List of whitelisted links and usernames,
   * For channels and groups to stop warning users for them.
   * Pass false to whitelist all links and channels.
   */
	excludeLinks: [
		'BestOfSawada',
		'TheDevs',
		'RecapTime',
		'ThePinsTeam_TransparencyHub',
		'ThePinsTeam_FedLogs',
		'Telegram',
		'DevChangeLogs_byMPTeam',
		'UpdatesFromThePinsTeam',
		'Kuys_Potpot',
      'SpamWatch',
      "SpamWatchSupport",
		'SpamWatchFederationLogs',
		'madebythepins.tk',
		'handbooksbythepins.gq',
		'recaptime.tk',
    'BestOfSitiSchu',
    'GlitchCommunityChat',
    'SawadaMeetsWeirdoes',
    'ThePinsTeam_FedSupport',
    "OrangeFoxOT",
    'IntellivoidDev'
	],

	/**
   * @type {ms}
   * Don't count warns older than this value towards automatic ban.
   * [Look at typedef above for details.]
   */
	expireWarnsAfter: Infinity,

	/**
   * @type {InlineKeyboard}
   * Inline keyboard to be added to reply to /groups.
   * We use it to display button opening our webpage.
   */
	groupsInlineKeyboard: [],

	numberOfWarnsToBan: 5,

	/**
   * @type {string[]}
   * List of plugin names to be loaded.
   * See Readme in plugins directory for more details.
   */
	plugins: [ 'codeblockChecker', 'antiscam', "anti-sandland" ],

  	/**
	 * @type { {token: string | false, host: string | undefined } | false}
	 * SpamWatch API Integration
	 *
	 * To enable this, an token for https://api.spamwat.ch or your API instance is required.
	 *
	 * To disable, comment this out or use 'false'.
	 */
	spamwatch: {

		/**
     * @type {(string | false )}
     *
     * SpamWatch API token.
     * To get a fresh token, join SpamWatch Support in Telegram.
     *
     * You should pull your API token from env, not here!
     * Pasting it here is a bad pratice.
     */
		token: process.env.SPAMWATCH_API_TOKEN,

		/**
     * @type {string | undefined}
     * SpamWatch API host. Defaults to 'https://api.spamwat.ch'.
     * Unless you have issues while connecting there, keep it unchanged.
     */
		host: process.env.SPAMWATCH_API_HOST,
	},

	/**
   * @type {InlineKeyboard}
   * Inline keyboard to be added to warn message.
   * We use it to display button showing our rules.
   */
	warnInlineKeyboard: [ [ { text: 'Read the rules!', url: 'https://t.me/ThePinsTeam_GeneralChatRules' } ] ],
};

module.exports = Object.freeze(config);
