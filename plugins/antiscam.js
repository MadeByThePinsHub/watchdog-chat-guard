'use strict';

const Composer = require('telegraf/composer');

// Not literally bad words, we did some RegExp to block scammers from using these.
const NotToScam = [
	/@onlypremiumsign/,
	/@PRO_TRADE_/,
	/@premium_call/,
	/@dropatz/,
	/@cloudrop/,
	/♻️ @/,
	/👉 TUTORIAL/
].map(regex => text => regex.exec(text));


module.exports = Composer.match(NotToScam, ctx =>
	Promise.all([
		ctx.ban({
			admin: ctx.botInfo,
			reason: 'Scam Link detected',
			userToBan: ctx.from
		}),
		ctx.deleteMessage()
	]));