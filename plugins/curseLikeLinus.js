'use strict';

const Composer = require('telegraf/composer');

// Warning: Contains swearing
const SwearLikeAnLinus = [
	/[Hh]oly [Aa]ctual/
].map(regex => text => regex.exec(text));


module.exports = Composer.match(SwearLikeAnLinus, ctx =>
    Promise.all([
      	ctx.deleteMessage().catch(() => null);
	      return ctx.warn({
		      admin: ctx.botInfo,
		      reason: 'Swearing like a Linus Sebastian/Torvalds',
		      userToWarn: ctx.from,
		      mode: 'auto',
	});
]))