# Plugins #

Plugins let you extend the bot with new functionality
without touching source code of the bot.

## Using plugins ##

To use a plugin, put it in this directory and add it's name
to `plugins` array in `config.js`.

If `plugins` is undefined, no plugins are loaded.
However, this behavior may change, don't rely on it.
If you want no plugins to be loaded, explicitly set it to empty array.


## Creating a plugin ##

Plugin is basically "requirable"
(JS file, or directory with `index.js`)
which exports a valid Telegraf handler
(usually function or `Composer` instance).

Plugins are similar to [micro-bot] bots.


## Plugins shipped with WCG ##

In Watchdog Chat Guard, we nuked the `.gitignore` file in this directory
to make sure that everything is packaged without asking people to pull our plugins
manually somewhere.

- `anti-sandland.js` - Checks if a user sent a Arabic or Persian text and automatically blocks across
the network.
- `antiscam.js` - 
- `codeblockChecker.js` - Checks whenever your code is too long and prompts you to use GitHub Gists
(or other snippet hosting service) and send that link instead.
- `curseLikeLinus.js` - Automaticlly warn people if you're swearing like Linus Sebastian/Linus Torvalds. (**WARNING: NOT FOR KIDS**)

[micro-bot]: https://github.com/telegraf/micro-bot
