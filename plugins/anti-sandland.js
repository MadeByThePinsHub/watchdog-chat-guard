'use strict';

const C = require('telegraf/composer');

// Utils
const o = (f, g) => x => f(g(x));
const both = (a, b) =>
	(...args) =>
		a(...args) && b(...args);

// Telegraf utils
const text = ctx => (
	ctx.message.entities ||
	ctx.message.caption_entities ||
	[]
).reduce(
	(acc, entity) =>
		entity.type === 'url'
			? acc.slice(0, entity.offset) +
				acc.slice(entity.offset + entity.length)
			: acc,
	ctx.message.text ||
	ctx.message.caption ||
	'');

const isNonAdmin = ctx =>
	!ctx.state.isMaster && ctx.from.status !== 'admin';

// Text utils
const isSandland = c =>
	c >= '\u0600' && c <= '\u06FF';

const sandlandAmount = s =>
	s.split('').filter(isSandland).length / s.length;

const banAmount = len =>
	(amount => amount <= 0 ? 1 : amount)
	(4 / (len - 2) + 0.2);

// Main hook
const shouldBan = text =>
	sandlandAmount(text) >= banAmount(text.length);

// Woop woop
module.exports = C.optional(
	both(
		o(shouldBan, text),
		isNonAdmin),
	ctx =>
		Promise.all([
			ctx.deleteMessage(),
			ctx.ban({
				admin: ctx.botInfo,
				reason: 'Persian/Arabic text detected',
				userToBan: ctx.from
			})
		]));