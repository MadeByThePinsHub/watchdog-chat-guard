## Use the latest Ubuntu LTS instead.
FROM ubuntu:latest

## Copy the repo files into the '/app' directory.
COPY . /app
WORKDIR /app

## Because we're using Node.js in Ubuntu, install Git SCM first!
RUN apt-get update && apt-get install -y -q git

## In lines 13 to 42, we'll install Node Version Manager in Docker.
## See this issue in https://stackoverflow.com/questions/25899912/how-to-install-nvm-in-docker for details
# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Set debconf to run non-interactively
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Install base dependencies
RUN apt-get update && apt-get install -y -q --no-install-recommends \
        apt-transport-https \
        build-essential \
        ca-certificates \
        curl \
        git \
        libssl-dev \
        wget \
    && rm -rf /var/lib/apt/lists/*

## You know it.
ENV NVM_DIR /usr/local/nvm # or ~/.nvm, depending on where you want.
ENV NODE_VERSION 12.16.2

# Install nvm with node and npm
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.35.3/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION --latest-npm \
    && nvm alias default $NODE_VERSION \
    && nvm use default

#
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/v$NODE_VERSION/bin:$PATH

## Install the dependencies
CMD nvm run 12.16.2 npm install

## Do some CI stuff
RUN npm ci --only=production

## Then kick off!
CMD nvm run 12.16.2 npm start
