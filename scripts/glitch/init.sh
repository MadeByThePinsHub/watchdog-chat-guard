#!/bin/sh

## Get the variables first!
TELEGRAM_API_TOKEN=$TELEGRAM_API_TOKEN
SPAMWATCH_API_TOKEN=$SPAMWATCH_API_TOKEN
SPAMWATCH_API_HOST=$SPAMWATCH_API_HOST

_term() {
  echo "[INFO] SIGTERM received, shutting down..."
  if [ -z ${PROJECT_DOMAIN} ]; then
    kill -TERM "$child" 2>/dev/null
  else
    echo "[RESURECT SOS] Sending SOS to Resurrect service..."
    curl -sSL "https://resurrect.glitch.me/$PROJECT_DOMAIN/optional/path/here" -o -
    kill -TERM "$child" 2>/dev/null
  fi
}

MAINTEANANCE_MODE=$MAINTEANANCE_MODE
if [[ $MAINTEANANCE_MODE == "enabled" ]]; then
  echo "[ERR] Maintainance mode enabled."
  exit 1
else
  echo "[INFO] Installing dependencies..."
  command npm i
  echo "[INFO] Starting up..."
  nodemon index.js
fi

child=$!
wait "$child"
